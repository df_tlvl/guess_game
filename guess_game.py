import random
from util import int_input

name = input('Hello, what\'s your name? ')
secret_number = random.randint(0, 100)
guess_count = int_input('How fast can you guess the number? ')
guess = int_input('Try to guess the number: ')
guess_count -= 1
while guess != secret_number and guess_count:
    if guess > secret_number:
        print('Answer is less')
    else:
        print('Answer is greater')
    guess = int_input('Try again to guess the number: ')
    guess_count -= 1 # guess_count = guest_count + 1
if guess == secret_number:
    print('Congratulations, ' + name + '!')
else:
    print('Try better next time!')
