def int_input(condition):
    while True:
        raw_input = input(condition)
        try:
            result = int(raw_input)
            return result
        except ValueError:
            print('Please type integer values!')
